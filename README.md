# 🤖 Cardinal
> Cardinal is a discord bot made within DBM (exept musicbot)

## 🚀 Selfhosting
I provide support to selfhosting as long as **the code is the original one found at this repository**.If you dont want to selfhost
you can use the official bot. **[Invite](https://discord.com/oauth2/authorize?client_id=748976784216686624&scope=bot&permissions=8)** Please do keep in mind that the bot may be closed from time-to-time for maintenance.

## Requirements

1. Discord Bot Token **[Guide](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot)**
2. YouTube Data API v3 Key **[Guide](https://developers.google.com/youtube/v3/getting-started)**  
2.1 **(Optional)** Soundcloud Client ID **[Guide](https://github.com/zackradisic/node-soundcloud-downloader#client-id)**
3. Node.js v12.0.0 or newer

## 🚀 Getting Started

```
git clone https://gitlab.com/kreato/cardinal.git
cd cardinal
npm i
```

After installation finishes you can use `node bot.js` to start the bot.
⚠️**Note:You need to enable Presence Intent for it to work**⚠️

## ⚙️ Configuration

open `config.json` and fill out the values:
then open `settings.json` and fill out the values:
at last open `.env` and fill out the values:
⚠️**Note: Never commit or share your token or api keys publicly** ⚠️
config.json
```json
{
  "TOKEN": "",
  "YOUTUBE_API_KEY": "",
  "SOUNDCLOUD_CLIENT_ID": "",
  "MAX_PLAYLIST_SIZE": 10,
  "PREFIX": ".",
  "PRUNING": false
}
```
Settings.json
```json
 {"token":"INSERT TOKEN HERE","client":"INSERT BOT CLIENT ID ","tag":".","case":"false","separator":"\\s+","modules":{"discord.js":["",true],"fstorm":["",true],"jimp":["latest",true],"libsodium-wrappers":["",true],"opusscript":["latest",true],"ytdl-core":["",true],"alexa-bot-api":["",true],"scrape-yt":["",true],"speedtest-net":["latest",true],"simple-youtube-api":["",true],"node-cron":["latest",true],"ytsr":["latest",true],"node-opus":["",true],"@discordjs/opus":["latest",true],"request":["",true],"ffmpeg-static":["latest",true],"google-translate-api":["",true],"ytdl":["",true],"google-tts-api":["latest",true],"node-fetch":["",true],"chalk":["",true],"ejs":["",true]},"ownerId":"OWNER ID","Bot Intents":{"customData":{"Bot Intents":{"intents":32511}}},"DBM Dashboard":{"customData":{"DBM Dashboard":{"port":"3000","clientSecret":"CLIENT SECRET","callbackURL":"http://localhost:3000/dashboard/callback","owner":"OWNER ID","supportServer":"https://discord.gg/"}}},"Bot Dashboard":{"customData":{"Bot Dashboard":{"port":"3000","clientSecret":"","callbackURL":"http://localhost:3000/dashboard/callback","owner":"OWNER ID","supportServer":"https://discord.gg/gay"}}}} 
 ```
## 📝 Features & Commands

> Note: The default prefix is '.'
> it has;
*Giveaway system*
*Music system*
*Level up system*
*Moderation features*
*Invite manager*
*and more!*


## 🤝 Contributing

1. [Fork the repository](https://gitlab.com/kreato/cardinal/-/forks/new)
2. Clone your fork: `git clone https://gitlab.com/your-username/cardinal.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -am 'Add some feature'`
5. Push to the branch: `git push origin my-new-feature`
6. Submit a pull request

## 📝 Credits

[@iCrawl](https://github.com/iCrawl) For the queue system used in this application which was adapted from [@iCrawl/discord-music-bot](https://github.com/iCrawl/discord-music-bot)

[@eritislami](https://github.com/eritislami) For the music system [@eritislami/evobot](https://github.com/eritislami/evobot/discord-music-bot)

[@dbm-network](https://github.com/dbm-network) for the DBM Mods that made this bot awesome [@dbm-network/mods](https://github.com/dbm-network/mods)


## ⚠️ Warning
This bot is still Work-In-Progress and it might have bugs/crash.Please report any of the bugs to me using discord(Kreato#5771) or through issues.
